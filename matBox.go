package okofrm24

/*
	Because building a Frame can be cpu intensive, I'm adding the matbox type to
	enable sending-off to a different machine to build the full Frame---
*/

import (
	"encoding/gob"
	"fmt"
	"net"
	"strings"
	"time"

	"gocv.io/x/gocv"
	"okotek.ai/x/okocfg"
)

// Simple little struct to hold the OpenCV matrix and some basic metadatad
type MatBox struct {
	Mat     gocv.Mat
	CapTime uint64
	UName   string
	UPass   string
}

func MakeMatBox(mat gocv.Mat, CapTime uint64, UName string, UPass string) MatBox {

	//Later, we'll do sanchecks here

	//Generate and send off
	return MatBox{
		mat,
		CapTime,
		UName,
		UPass,
	}
}

func (mtb MatBox) ConvertToFrame() Frame {

	retFrm := genBlank()
	incMet := genBlankMeta()

	incMet.Uname = mtb.UName
	incMet.Upass = mtb.UPass
	incMet.Unano = mtb.CapTime
	incMet.MType = mtb.Mat.Type()
	incMet.Tags = []Tag{}
	incMet.Width = mtb.Mat.Cols()
	incMet.Height = mtb.Mat.Rows()
	incMet.Chans = mtb.Mat.Channels()
	incMet.Name = ""

	retFrm.Bytes = mtb.Mat.ToBytes()

	return retFrm
}

// Send this MatBox to a Neighbor that we're connected to
func (mtb MatBox) sendToProcessor() {

	//Get an up-to-date configuration factory
	cfg := okocfg.GetDatPuller()
	procAddrs := strings.Split(cfg("okocam")["processorAddresses"], ",")
	for _, pAdd := range procAddrs {

		go mtb.MatBoxSendoff(pAdd)
	}
}

func (mtb MatBox) MatBoxSendoff(targ string) {
	if netCon, conErr := net.Dial("tcp", targ); conErr != nil {
		fmt.Println("connection error with MatBoxSendoff:", conErr)
		time.Sleep(100 * time.Millisecond) //Stop us from spamming the network and CPU
		mtb.MatBoxSendoff(targ)
		return
	} else {
		enc := gob.NewEncoder(netCon)
		if encErr := enc.Encode(mtb); isFatalEncodeError(encErr) {

		} else {
			fmt.Println("Encode sendoff error in MatBoxSendoff: ", encErr)
		}

	}
}

// NOTE: Lots to complete here
// Tell us to either give up or continue on, based on what kind of error we have -- copied encode source for reference
func isFatalEncodeError(input error) bool {

	//inputStr := input.Error()
	iSt := input.Error()

	switch {
	case iSt == "gob: cannot encode nil value":
		return true

	default:
		return true

	}

	/*
	   // EncodeValue transmits the data item represented by the reflection value,
	   // guaranteeing that all necessary type information has been transmitted first.
	   // Passing a nil pointer to EncodeValue will panic, as they cannot be transmitted by gob.
	   func (enc *Encoder) EncodeValue(value reflect.Value) error {
	   	if value.Kind() == reflect.Invalid {
	   		return errors.New("gob: cannot encode nil value")
	   	}
	   	if value.Kind() == reflect.Pointer && value.IsNil() {
	   		panic("gob: cannot encode nil pointer of type " + value.Type().String())
	   	}

	   	// Make sure we're single-threaded through here, so multiple
	   	// goroutines can share an encoder.
	   	enc.mutex.Lock()
	   	defer enc.mutex.Unlock()

	   	// Remove any nested writers remaining due to previous errors.
	   	enc.w = enc.w[0:1]

	   	ut, err := validUserType(value.Type())
	   	if err != nil {
	   		return err
	   	}

	   	enc.err = nil
	   	enc.byteBuf.Reset()
	   	enc.byteBuf.Write(spaceForLength)
	   	state := enc.newEncoderState(&enc.byteBuf)

	   	enc.sendTypeDescriptor(enc.writer(), state, ut)
	   	enc.sendTypeId(state, ut)
	   	if enc.err != nil {
	   		return enc.err
	   	}

	   	// Encode the object.
	   	enc.encode(state.b, value, ut)
	   	if enc.err == nil {
	   		enc.writeMessage(enc.writer(), state.b)
	   	}

	   	enc.freeEncoderState(state)
	   	return enc.err
	   }// EncodeValue transmits the data item represented by the reflection value,
	   // guaranteeing that all necessary type information has been transmitted first.
	   // Passing a nil pointer to EncodeValue will panic, as they cannot be transmitted by gob.
	   func (enc *Encoder) EncodeValue(value reflect.Value) error {
	   	if value.Kind() == reflect.Invalid {
	   		return errors.New("gob: cannot encode nil value")
	   	}
	   	if value.Kind() == reflect.Pointer && value.IsNil() {
	   		panic("gob: cannot encode nil pointer of type " + value.Type().String())
	   	}

	   	// Make sure we're single-threaded through here, so multiple
	   	// goroutines can share an encoder.
	   	enc.mutex.Lock()
	   	defer enc.mutex.Unlock()

	   	// Remove any nested writers remaining due to previous errors.
	   	enc.w = enc.w[0:1]

	   	ut, err := validUserType(value.Type())
	   	if err != nil {
	   		return err
	   	}

	   	enc.err = nil
	   	enc.byteBuf.Reset()
	   	enc.byteBuf.Write(spaceForLength)
	   	state := enc.newEncoderState(&enc.byteBuf)

	   	enc.sendTypeDescriptor(enc.writer(), state, ut)
	   	enc.sendTypeId(state, ut)
	   	if enc.err != nil {
	   		return enc.err
	   	}

	   	// Encode the object.
	   	enc.encode(state.b, value, ut)
	   	if enc.err == nil {
	   		enc.writeMessage(enc.writer(), state.b)
	   	}

	   	enc.freeEncoderState(state)
	   	return enc.err
	   }

	*/

}

func MatBoxListen(output chan MatBox, port string) {

	if ln, lnErr := net.Listen("tlc", port); lnErr != nil {
		fmt.Println("Error listening: ", lnErr)
		time.Sleep(450 * time.Millisecond)
		MatBoxListen(output, port)
		return
	} else {

		getBox := MatBox{}

		for {
			if con, conErr := ln.Accept(); conErr != nil {
				fmt.Println("Error getting listen con: ", conErr)
				time.Sleep(450 * time.Millisecond)
				MatBoxListen(output, port)
				return
			} else {
				dec := gob.NewDecoder(con)
				if decErr := dec.Decode(getBox); decErr != nil {
					fmt.Println("Error getting decode:", decErr)
					time.Sleep(450 * time.Millisecond)
					MatBoxListen(output, port)
				} else {
					output <- getBox
				}

			}
		}
	}

}
