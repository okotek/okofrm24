package okofrm24

import (
	"crypto/sha256"
	"image"
	"strconv"
	"time"

	gocv "gocv.io/x/gocv"
)

type Tag struct {
	Name     string
	Version  string
	Location image.Rectangle
}

type Meta struct {
	Uname     string
	Upass     string
	Unano     uint64 // Timestamp -- Unix Nano
	MType     gocv.MatType
	Tags      []Tag
	Width     int
	Height    int
	Chans     int
	Name      string //Unique string that identifies this Frame or Clip
	Timestamp uint64
}

// Copies "static" meta values from m2 to m1
func copyStaticMeta(m1, m2 *Meta) {
	m1.Uname = m2.Uname
	m1.Upass = m2.Upass
	m1.MType = m2.MType
	m1.Width = m2.Width
	m1.Chans = m2.Chans
	m1.Height = m2.Height
}

func genBlankMeta() Meta {
	return Meta{
		"",
		"",
		uint64(time.Now().UnixNano()),
		gocv.MatType(0),
		[]Tag{},
		0,
		0,
		0,
		"",
		0,
	}
}

// Consume the metadata of a gocv Mat
func (mt Meta) ConsumeMat(input gocv.Mat) error {
	mt.Unano = uint64(time.Now().UnixNano())
	mt.MType = input.Type()
	mt.Width = input.Cols()
	mt.Height = input.Rows()
	mt.Chans = input.Channels()

	return nil
}

// =============================================
type Frame struct {
	Bytes []byte
	Met   Meta
}

// =============================================a

// Generate a frame type that bas been fullypopulated
func GenFullFrame(
	mat gocv.Mat,
	uName string,
	uPass string,
	tags []Tag,
) Frame {

	//Run san-checks
	//...

	//Simple return
	retFrm := Frame{
		mat.ToBytes(),
		Meta{uName,
			uPass,
			uint64(time.Now().UnixNano()),
			mat.Type(),
			tags,
			mat.Cols(),
			mat.Rows(),
			mat.Channels(),
			"",
			uint64(time.Now().UnixNano()),
		},
	}

	retFrm.runHash()

	return retFrm
}

// Generate a hash of the frame's byte data combined with time data to crate a unique identifier
func (frm *Frame) runHash() error {
	/*
		fmt.Println("HASH NOT IMPLEMENTED YET")
		psuedoHash := strconv.Itoa(int(time.Now().UnixNano()))
		frm.Met.Name = psuedoHash
	*/

	timeString := strconv.Itoa(int(frm.Met.Timestamp))
	hash1 := sha256.Sum256(frm.Bytes)
	hash2 := sha256.Sum256([]byte(frm.Met.Uname + frm.Met.Upass + timeString))

	frm.Met.Name = string(hash1[:]) + "||" + string(hash2[:]) + "||" + timeString

	return nil //NOTE: SHIM
}

type SubFrame struct {
	ByteData []byte
	Tags     []Tag
}

// Parallel soon? This good place for that.
func (cp Clip) ToFrames() []Frame {
	templateFrame := genBlank()
	retSlc := []Frame{}

	templateFrame.Met = cp.Met

	for _, frm := range cp.Frames {
		templateFrame.Bytes = frm.ByteData
		templateFrame.Met.Tags = frm.Tags
		retSlc = append(retSlc, templateFrame)
	}

	return retSlc
}

type Clip struct {
	Met    Meta
	Frames []SubFrame
}

func (clp Clip) GetDetections() []Tag {
	var ret []Tag
	for _, frm := range clp.Frames {
		ret = append(ret, frm.Tags...)
	}
	return ret
}

func genBlank() Frame {
	return Frame{
		[]byte{},
		Meta{
			"",
			"",
			uint64(time.Now().UnixNano()),
			gocv.MatType(0),
			[]Tag{},
			0,
			0,
			0,
			"",
			uint64(time.Now().UnixNano()),
		},
	}
}

// Build frame from image and return an unhashed frame from that image
func BuildFromImg(targ, uname, upass string) (Frame, error) {

	var buildError error
	if frmMat /*, buildError*/ := gocv.IMRead(targ, 1); buildError != nil {
		return genBlank(), buildError
	} else {
		return Frame{
			frmMat.ToBytes(),
			Meta{
				uname,
				upass,
				uint64(time.Now().UnixNano()),
				frmMat.Type(),
				[]Tag{},
				frmMat.Cols(),
				frmMat.Rows(),
				frmMat.Channels(),
				"",
				uint64(time.Now().UnixNano()),
			}}, nil
	}

}

/*

	type Meta struct {
		Uname  string
		Upass  string
		Unano  uint64 // Timestamp -- Unix Nano
		MType  gocv.MatType
		Tags   []Tag
		Width  int
		Height int
		Chans  int
		Name   string //Unique string that identifies this Frame or Clip
	}

*/
