package okofrm24

import (
	"fmt"
	"testing"

	gocv "gocv.io/x/gocv"
)

func TestBuildFrame(t *testing.T) {
	var readError error
	if readMat /*, readError*/ := gocv.IMRead("lenna.png", 1); readError != nil {
		fmt.Println("Failed read test:", readError)
	} else {
		blankFrame := genBlank()
		imgFrame := GenFullFrame(readMat, "test", "test", []Tag{})

		fmt.Println(blankFrame, imgFrame)
	}

}

func TestHash(t *testing.T) {
	var readError error
	if readMat /*, readError*/ := gocv.IMRead("lenna.png", 1); readError != nil {
		fmt.Println("Failed to read in Hash test:", readError)
	} else {
		imgFrame := GenFullFrame(readMat, "test", "test", []Tag{})

		imgFrame.runHash()

		fmt.Println(imgFrame.Met.Name)
	}
}
